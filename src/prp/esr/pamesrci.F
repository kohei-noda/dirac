!dirac_copyright_start
!      Copyright (c) by the authors of DIRAC.
!
!      This program is free software; you can redistribute it and/or
!      modify it under the terms of the GNU Lesser General Public
!      License version 2.1 as published by the Free Software Foundation.
!
!      This program is distributed in the hope that it will be useful,
!      but WITHOUT ANY WARRANTY; without even the implied warranty of
!      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!      Lesser General Public License for more details.
!
!      If a copy of the GNU LGPL v2.1 was not distributed with this
!      code, you can obtain one at https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html.
!dirac_copyright_end

C&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
C  /* Deck esr_init */
      SUBROUTINE ESR_INIT(WF,IPRINT,WORK,KFREE,LFREE)
C***********************************************************************
C
C     Initialize ESR calculation: find out what to do
C
C     Input : WF - wave function type
C
C     Output: in common blocks
C
C     Author: Hans Joergen Aa. Jensen
C
C***********************************************************************
#include "implicit.h"
#include "priunit.h"
C
#include "dcbgen.h"
#include "dcborb.h"
#include "dcbdhf.h"
#include "dcbopt.h"
#include "dgroup.h"
C
      DIMENSION WORK(*)
      CHARACTER WF*4
C
      CALL QENTER('ESR_INIT')
C
      IPROPT = IPRINT
      IF (WF(1:3) .NE. 'DHF' .AND. WF .NE. 'DFT') THEN
         ! note March 2022:: WF is not properly implemented in property module,
         ! except for RELCCSD. Otherwise it is always DHF
         ! if not set explicitly in input, no matter if DFT or KRMC ...
         CALL QUIT('ESR only implemented for DHF and DFT so far')
      END IF
C
      IF (WF(1:4) .EQ. 'DHF1') THEN
C        ... special input for using old code for a single open shell
         IF (NOPEN .NE. 1) THEN
            CALL QUIT('"DHF1" is only for NOPEN .eq. 1')
         END IF
         GO TO 9999
      END IF
C
C
C     Include spin polarization in CI ?? If yes redefine GAS spaces
C     NASH(:), NASHT, NACTEL, NAELEC etc. are all reset in THDFORB
C
      CALL TDHFORB('ESRCI')
      CALL SETDC2(0)

C
C     Define dcbidx.h
C
      CALL SETDCBIDX()
C
C     Initialize IOPT_*(2) for generation of FC and FV matrices
C     (used in GASCIP_FULLCI for getting FC)
C
      DO I = 1,2
         IOPT_ISYMOP(I) = 1
         IOPT_IFCKOP(I) = 1
         IOPT_IHRMOP(I) = 1
      END DO
C
C     We reuse LUKRMC=50 and RSET_KRMC from krmcscf module
C     for esr_resolve information (but with file name
C     ESR_RESOLVE instead of KRMCSCF).
C
      LUKRMC = 50
      CALL OPNFIL(LUKRMC,'ESR_RESOLVE','UNKNOWN','ESR_INIT')
      CALL NEWLAB('*ESR_RES',LUKRMC,LUPRI)
C
C     **** CI problem is defined by transfer of information in
C          CALL TDHFORB in beginning of PRP module
C
C     **** Find necessary CI information:
C          - find number of determinants,
C          - generate list of determinants.
C          Note: only GASCIP program implemented for ESR_RESOLVE !
C
      IOPT_SYMMETRY = -1
C     ... code for including all symmetries
      CALL RSETCI('GASCIP',WORK,KFREE,LFREE)
C
C     This is CI, so no orbital rotations:
C
      NZXOPE = 0
      NZHOPE = 0
      LZXOPE = NZHOPE*2
      NZXOPP = 0
      LZXOPP = NZXOPP*2
C
C     Write information to LUKRMC
C
      CALL RSET_KRMC(WORK)
C
 9999 CALL QEXIT('ESR_INIT')
      RETURN
      END
C&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
C  /* Deck esr_resolve */
      SUBROUTINE ESR_RESOLVE(CMO,IPRINT,N_CIROOTS,MAX_CI_IT,THR_CI,
     &           WORK,K_EIGVAL,K_EIGVEC,KFREE,LFREE)

C***********************************************************************
C
C     Resolve CI for ESR with open shell DHF code
C
C     Input : CMO - MO coefficents
C
C     Output: K_EIGVAL - WORK(K_EIGVAL) has eigenvalues
C             K_EIGVEC - WORK(K_EIGVEC) has eigenvectors
C
C     Author: Hans Joergen Aa. Jensen
C     (Based on GASCIP_STARTVEC by Joern Thyssen June 2000)
C
C***********************************************************************
#include "implicit.h"
#include "priunit.h"
C
      PARAMETER (D0 = 0.0D0)
C
C dcbham.h : HFXFAC, HFXMU
C dcbesr.h : THR_CIESR, THR_CVEC_ESR
C dcbgascip.h : IMK2(), NMK2(), MINMK2, MAXMK2
#include "dcbgen.h"
#include "dcbham.h"
#include "dcborb.h"
#include "dcbopt.h"
#include "dcbesr.h"
#include "dcbgascip.h"
#include "dgroup.h"
!#include <krmcluci_inf.h>
C
      DIMENSION CMO(*), WORK(*)
      CHARACTER SZTRA(2,4)*200
C
      CALL QENTER('ESR_RESOLVE')

      IF (NZCONF .EQ. 2 .AND. N_CIROOTS .lt. 0) THEN
      ! avoid GASCIP -- this enables DFT calculations for doublet
         N_CIROOTS = 2
         CALL ESR_RESOLVE_2(IPRINT,
     &           WORK,K_EIGVAL,K_EIGVEC,KFREE,LFREE)
         GO TO 9000
      END IF

      KFRSAV = KFREE
C
      IPROPT =  MAX(IPRINT,3)
C
C     ***************************************************
C     *** Which integrals to calculate ? ****************
C     ***************************************************
C
      CALL INTCON(IOPT_INTFLG,IOPT_INTBUF,IOPT_INTDEF,D0,
     &        OPT_CNVINT,9999,IOPT_ITRINT,OPT_INTTYP)
C
      WRITE(LUPRI,9020) OPT_INTTYP
 9020 FORMAT(/' (ESR_RESOLVE) Integrals for CI: ',A)
C
C
C     ***************************************************
C     *** Transform two-electron integrals **************
C     ***************************************************
C
      CALL GETTIM(CPUITR1,WALLITR1)
C
      JTRLVL = 0
C     ... only (aa|aa) integrals
      CALL INISZT(JTRLVL,SZTRA,.FALSE.,IDUMMY)
      OPT_NOPFQ = .TRUE.
      CALL MEMGET2('INTE','IBEIG',KIBEIG,NORBT,WORK,KFREE,LFREE)
      CALL IZERO(WORK(KIBEIG),NORBT)
      CALL RTRACTL(CMO,WORK(KIBEIG),SZTRA,WORK(KFREE),LFREE)
      CALL MEMREL('ESR_RESOLVE AFTER RTRACTL',
     &             WORK,1,KFRSAV,KFREE,LFREE) 
C
C     **** Resolve CI problem -- find eigenvalues and eigenvectors
C
C     - Set to full exchange, HFXFAC=1.0d0; this is 100% wave function
C       theory, also if the orbitals are generated with a DFT functional.

      HFXFAC_save = HFXFAC
      HFXMU_save  = HFXMU
      HFXFAC  = 1.0D0
      HFXMU   = 0.0D0
      IF (MAX_CI_IT .GE. 0) THEN
C
C        MAX_CI_IT Davidson CI iterations.
C        ( MAX_CI_IT < 0 means do FULL CI )
C
         L_EIGVAL = N_CIROOTS
         L_EIGVEC = N_CIROOTS*NZCONF*NZ_in_CI ! final CI eigen vectors
         L_EIGVXX = N_CIROOTS*NZCONF*NZ_in_CI ! CI eigen vectors from GASCIP_RCISTD
         CALL MEMGET2('REAL','EIGVAL',K_EIGVAL,L_EIGVAL,
     &      WORK,KFREE,LFREE)
         CALL MEMGET2('REAL','EIGVXX',K_EIGVXX,L_EIGVEC,
     &      WORK,KFREE,LFREE)
!
!        OBS!  K_EIGVAL and K_EIGVEC is NON-STANDARD POINTER FEATURE !!!!
!        assign pointer to CI vectors which is in the argument list of this subroutine and
!        needs to be passed upwards to the calling routine.
         K_EIGVEC = K_EIGVXX
         
         IF (USE_KRAMERS_CONJ .AND. N_CIROOTS .EQ. 2) THEN
            write(lupri,'(/A/)')
     &      'ESR_RESOLVE: Using Kramers conjugation of CI vector'
            N_ROOTS = -1
         ELSE
            N_ROOTS = N_CIROOTS
         END IF

         IPR_GASCIP = max(IPRESR,2)   ! print always timing of each micro iteration
         MAX_CI_IT_local  = MAX_CI_IT ! GASCIP_RCISTD modifies MAX_CI_IT
         CALL GASCIP_RCISTD(N_ROOTS, THR_CIESR, MAX_CI_IT_local,
     &                     WORK(K_EIGVAL),WORK(K_EIGVXX),NZCONF,
     &                     WORK(KZCONF), CMO,
     &                     THR_CVEC_ESR, 0, ICONV_TOT,
     &                     THRPCI_ESR,IPR_GASCIP,WORK(KFREE), LFREE)

C        CALL GASCIP_RCISTD(N_ROOTS,RTHRCI,MAX_CI_IT,
C     &                     ECI,CVECS,NDET,IDET,CMO,
C     &                     THR_CVEC_ESR,ISTATE,ICONV_TOT,
C     &                     THR_PCI,IPRINT,WORK,LWORK)

         IF (ICONV_TOT .LE. 0) THEN
            CALL QUIT('ESR CI not converged.')
         END IF
C
C        Sort CI eigenvectors from (real C1, imag C1, real C2, etc)
C        to (real C1, real C2, ..., imag C1, imag c2, ...) for later use in QTRANS
C
         IF (NZ_in_CI .EQ. 2) THEN
            CALL MEMGET2('REAL','EIGVX2',K_EIGVX2,L_EIGVEC,
     &         WORK,KFREE,LFREE)
            N_ROOTS = abs(N_ROOTS)
            J_EIGVEC_R = K_EIGVX2 - 1
            J_EIGVEC_I = J_EIGVEC_R + N_CIROOTS*NZCONF
            J_EIGVXX_R = K_EIGVXX - 1
            J_EIGVXX_I = J_EIGVXX_R + NZCONF
            DO J = 1,N_ROOTS
               DO I = 1,NZCONF
                  WORK(J_EIGVEC_R+I+(J-1)*NZCONF)=
     &            WORK(J_EIGVXX_R+I+(J-1)*NZCONFQ)
                  WORK(J_EIGVEC_I+I+(J-1)*NZCONF)=
     &            WORK(J_EIGVXX_I+I+(J-1)*NZCONFQ)
               END DO
            END DO
            call dcopy(L_EIGVEC,work(K_EIGVX2),1,work(K_EIGVXX),1)

            CALL MEMREL('ESR_RESOLVE AFTER CI STORAGE SORT',
     &               WORK,1,K_EIGVX2,KFREE,LFREE) 

         END IF

         IF (USE_KRAMERS_CONJ .AND. N_CIROOTS .EQ. 2) THEN
            write(lupri,'(//A/)')
     &     'Generating CI solution vector no. 2 by Kramers conjugation.'
            WORK(K_EIGVAL+1) = WORK(K_EIGVAL)
            K_EIGVEC_2 = K_EIGVEC + NZCONF
            CALL DZERO(WORK(K_EIGVEC_2),NZCONF)
            DO MK2 = MINMK2,MAXMK2
               NDET_MK2 = NMK2(MK2,3) 
               write(lupri,*) 'MK2, NDET_MK2',MK2,NDET_MK2
            IF (NDET_MK2 .GT. 0) THEN
               IMK2_1 = IMK2( MK2,3)
               IMK2_2 = IMK2(-MK2,3)
               write(lupri,*) 'IMK2_1, IMK2_2',IMK2_1,IMK2_2
               J1_EIGVEC = K_EIGVEC   + IMK2_1 - 1
               J2_EIGVEC = K_EIGVEC_2 + IMK2_2 - 1
               DO I = 1,NDET_MK2
                  WORK(J2_EIGVEC + I) = WORK(J1_EIGVEC + I)
               END DO
               IF (NZ_in_CI .EQ. 2) THEN
               J1_EIGVEC = J1_EIGVEC + N_CIROOTS*NZCONF
               J2_EIGVEC = J2_EIGVEC + N_CIROOTS*NZCONF
               DO I = 1,NDET_MK2
                  WORK(J2_EIGVEC + I) = -WORK(J1_EIGVEC + I) ! time-reversal operator includes complex conjugation
               END DO
               END IF
            END IF
            END DO
            IF (NZ_in_CI .eq. 1) THEN
            WRITE(LUPRI,'(//A/A,I3,A,I3/A)')
     &      ' ============================================',
     &      ' (GASCIP_RCISTD) CI solution vector',2,' of ',2,
     &      ' ============================================'
            CALL GASCIP_ANACI(WORK(K_EIGVEC_2),NZCONF,WORK(KZCONF),
     &                        THRPCI_ESR,WORK(KFREE),LFREE)
            END IF
!dbg        CALL OUTPUT(WORK(K_EIGVEC),1,NZCONF,1,2,NZCONF,2,1,LUPRI)
         END IF
C
      ELSE
C
C        Full CI.
C           
         L_EIGVAL = NZCONF
         L_EIGVEC = NZCONF*NZCONF * NZ_in_CI
         CALL MEMGET2('REAL','EIGVAL',K_EIGVAL,L_EIGVAL,
     &      WORK,KFREE,LFREE)
         CALL MEMGET2('REAL','EIGVEC',K_EIGVEC,L_EIGVEC,
     &      WORK,KFREE,LFREE)

         CALL GASCIP_FULLCI(NZCONF,WORK(KZCONF),CMO,
     &                     WORK(K_EIGVAL),WORK(K_EIGVEC),ECORE,
     &                     IPROPT,WORK,KFREE,LFREE)
C        CALL GASCIP_FULLCI(NDET,IDET,CMO,EIGVAL,EIGVEC,ECORE,
C     &                    IPRINT,WORK,LWORK)

      END IF
      HFXFAC  = HFXFAC_save
      HFXMU   = HFXMU_save
C           
C
Chj TODO: calculate and save an index vector with symmetry of each CI eigenvector
C
      CALL WRTKRMC(LUKRMC,'CI energ',WORK(K_EIGVAL),L_EIGVAL)
      CALL WRTKRMC(LUKRMC,'CI vecs ',WORK(K_EIGVEC),L_EIGVEC)
      CLOSE(LUKRMC,STATUS='KEEP')

#ifdef ESR_DEBUG
        WRITE(LUPRI,*)
     &   '(ESR_RESOLVE): Real part of the N_ROOTS CI vectors'
        CALL OUTPUT(WORK(K_EIGVEC),
     &              1,NZCONF,1,N_CIROOTS,NZCONF,N_CIROOTS,-1,LUPRI)
      IF (NZ_in_CI .eq. 2) THEN
        WRITE(LUPRI,*)
     &   '(ESR_RESOLVE): Imaginary part of the N_ROOTS CI vectors'
        CALL OUTPUT(WORK(K_EIGVEC+NZCONF*N_CIROOTS),
     &              1,NZCONF,1,N_CIROOTS,NZCONF,N_CIROOTS,-1,LUPRI)
      END IF
      DO I = 1,N_CIROOTS
         WRITE(lupri,*) 'I,WORK(K_EIGVAL-1+1)', I, WORK(K_EIGVAL-1+I) 
      END DO
#endif

! start programming print of nat.orb. occupations /1.nov.2012-hjaaj
!     IF (IPROPT .GE. 2) THEN
!        DO I = 1, N_CIROOTS
!           CALL  RGETNO(CREF,OCCNO,UNO,CMO,NATORB,MOISNO,save_1pdens,
!    &                  IPRESR,WORK,KFREE,LFREE)
!        END DO
!     END IF
C
 9000 CALL QEXIT('ESR_RESOLVE')
C
      RETURN
      END
C
      SUBROUTINE ESR_RESOLVE_2(IPRINT,
     &           WORK,K_EIGVAL,K_EIGVEC,KFREE,LFREE)
C
C     hjaaj Aug 2014
C     Special case: 2 determinants. Avoid GASCIP.
C                   This enables DFT calculations for doublet.
C
C***********************************************************************
#include "implicit.h"
#include "priunit.h"

      REAL*8  WORK(*)

#include "dcbdhf.h"
#include "dgroup.h"

      L_EIGVAL = 2
      L_EIGVEC = 4*NZ_in_CI
      CALL MEMGET2('REAL','EIGVAL',K_EIGVAL,L_EIGVAL,
     &      WORK,KFREE,LFREE)
      CALL MEMGET2('REAL','EIGVEC',K_EIGVEC,L_EIGVEC,
     &      WORK,KFREE,LFREE)
      CALL DZERO(WORK(K_EIGVAL),L_EIGVAL)
      CALL DZERO(WORK(K_EIGVEC),L_EIGVEC)
      WORK(K_EIGVAL)   = DHFERG
      WORK(K_EIGVAL+1) = DHFERG
      WORK(K_EIGVEC)   = 1.0D0
      WORK(K_EIGVEC+3) = 1.0D0
      RETURN
      END
! -- end of pamesrci.F --
