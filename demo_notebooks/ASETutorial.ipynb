{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "317e6df9",
   "metadata": {},
   "source": [
    "# ASE Interface\n",
    "\n",
    "*Lucas Visscher, Vrije Universiteit Amsterdam, 2025*\n",
    "\n",
    "Since release DIRAC25 we provide a Python interface to the Atomistic Simulation Environment that can be used to automate calculations.\n",
    "\n",
    "A prerequisite to this interface is to follow the installation instructions in the `ase_dirac` folder. As a minimum you need to do:\n",
    "\n",
    "1. install the ASE environment (if not already installed)\n",
    "2. add DIRAC's build directory to your PATH (as the scripts need the `pam` command)\n",
    "3. `cd ase_dirac; pip install --user -e ., cd ..` (to install the ase_dirac interface)\n",
    "\n",
    "After this step you may automate DIRAC calculations. The next code cell checks whether the interface is installed correctly. If it gives an error, you should check whether there was an issue with any of the three required steps above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa13a856",
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase import Atoms\n",
    "from ase.build import molecule\n",
    "from ase.units import Ha\n",
    "from ase_dirac import DIRAC"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39ace328-9cb0-4b1e-b741-09eda4b10e1e",
   "metadata": {},
   "source": [
    "As a first test we run an SCF calculation for the hydrogenchloride molecule and print the energy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84b46750-e06e-4ae9-bffa-257098b4e6f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "ase_molecule = molecule('HCl')\n",
    "ase_molecule.calc = DIRAC(\n",
    "              wave_function={'.scf': '',\n",
    "                             '*scf': {'.ergcnv': '1.E-8 1.E-6',\n",
    "                                      '.maxitr': '35'},\n",
    "                            },\n",
    "              molecule={'*basis': {'.default': 'cc-pVDZ'}},\n",
    "              label=label)\n",
    "print('The Hartree-Fock energy of HCl is ',ase_molecule.get_potential_energy() / Ha,' Hartree.')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f71e2f99",
   "metadata": {},
   "source": [
    "In the next cell we run a more extensive calculation in which we tabulate MP2 energies computed with different Hamiltonians for three small molecules. In this piece of Python code we also allow for potential failures of a calculation, using the `try` and `except` mechanism of Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b674710",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some simple small molecules taken from the G2 set in the ASE database\n",
    "molecules = ['H2O','N2','F2']\n",
    "# Defines the **HAMILTONIAN input block, note the two lines of input for the full DCG Hamiltonian\n",
    "hamiltonian_inputs = ['.nonrel','.x2cmmf','.gaunt\\n.dossss']\n",
    "# Labeling the different inputs with a descriptive string\n",
    "calculation_labels = ['NR-MP2','X2Cmmf-MP2','DCG-MP2']\n",
    "# Making an empty list to store results (energies) for each molecule\n",
    "results = []\n",
    "\n",
    "# Outermost loop over different molecules\n",
    "for mol in molecules:\n",
    "    ase_molecule = molecule(mol)\n",
    "    energy = []\n",
    "    # Loop over the three different Hamiltonians that we consider\n",
    "    for ham, calc in zip(hamiltonian_inputs,calculation_labels):\n",
    "        label=calc+'_'+mol\n",
    "        try:\n",
    "            ase_molecule.calc = DIRAC(\n",
    "              hamiltonian={ham: ''},\n",
    "              wave_function={'.scf': '', '.mp2': '',\n",
    "                             '*scf': {'.ergcnv': '1.E-8 1.E-6',\n",
    "                                      '.maxitr': '35'},\n",
    "                             '*mp2cal': {'.occup': 'all','.virtual': 'all'},\n",
    "                            },\n",
    "              molecule={'*basis': {'.default': 'cc-pVDZ'}},\n",
    "              label=label)\n",
    "            energy.append(ase_molecule.get_potential_energy() / Ha)\n",
    "        except:\n",
    "            print (f'Calculation {label} could not be run correctly, please check')\n",
    "            energy.append(0)\n",
    "    results.append(energy)\n",
    "    \n",
    "# Print results in a table\n",
    "print(' Møller-Plesset second order (MP2) energies computed with different Hamiltonians\\n')\n",
    "print(80*'-')\n",
    "print('  {:<15} {:>20} {:>20} {:>20}'.format('Molecule',*calculation_labels))\n",
    "print(80*'-')\n",
    "for mol, result in zip(molecules,results):\n",
    "    print ('  {:<15} {:>20.8f} {:>20.8f} {:>20.8f}'.format(mol,*result))\n",
    "print(80*'-')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a9ab758",
   "metadata": {},
   "source": [
    "Note that the relativistic energies are always lower (more negative) than the non-relativistic ones, and the good approximation that the X2Cmmf method provides, with energies slightly below the DCG reference due to the neglect of the Gaunt interaction in the default X2Cmmf scheme."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0cb1861-84c3-4240-8144-991c01dedefd",
   "metadata": {},
   "source": [
    "Note that the current interface is rather minimal. We plan to add more functionality in next releases but \n",
    "of course also welcome user contributions (simply make a pull request if you have added something useful to this interface)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5811c4bd-57e3-415f-9071-27065e6d5247",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
